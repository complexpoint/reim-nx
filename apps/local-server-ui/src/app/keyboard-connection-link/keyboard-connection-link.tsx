import { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import QRCode from 'react-qr-code';

import styles from './keyboard-connection-link.module.css';
import { serverContext } from '../server-context';

/* eslint-disable-next-line */
export interface KeyboardConnectionLinkProps {}

export const KeyboardConnectionLink = observer(
  (props: KeyboardConnectionLinkProps) => {
    const serverAPI = useContext(serverContext);

    return (
      <div className={styles['container']}>
        <div>{serverAPI.getKeyboardConnectionLink.value}</div>
        <QRCode value={serverAPI.getKeyboardConnectionLink.value} />
      </div>
    );
  }
);
export default KeyboardConnectionLink;
