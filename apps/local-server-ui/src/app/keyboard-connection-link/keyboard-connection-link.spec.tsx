import { render } from '@testing-library/react';

import KeyboardConnectionLink from './keyboard-connection-link';

describe('KeyboardConnectionLink', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<KeyboardConnectionLink />);
    expect(baseElement).toBeTruthy();
  });
});
