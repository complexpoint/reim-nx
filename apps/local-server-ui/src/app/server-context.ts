import React from "react";
import { makeAutoObservable } from 'mobx';
// API for interacting with Electron main process

declare global {
  interface Window {
    electron: {
      getIpAddress: () => Promise<string>;
      openDevTools: () => Promise<void>;
      getConnectionAddressForKeyboard: () => Promise<string>;
    };
  }
}
export const serverAPI = {
  ipAddress: {
    _value: '',
    reload() {
      this._value = 'loading...';
      window.electron.getIpAddress().then((ip) => this.set(ip))
    },
    set: function setIpAddress(ip: string): void { this._value = ip },
    get value() {
      if (this._value === '') {
        this.reload(); // async, so only observers will be updated
      }
      return this._value
    },
    toString() {
      return this.value;
    }
  },
  openDevTools(): Promise<void> { return window.electron.openDevTools() },
  getKeyboardConnectionLink: {
    _value: '',
    reload() {
      this._value = 'loading...';
      return window.electron.getConnectionAddressForKeyboard().then((address) => {
        this._value = address;
      })
    },
    get value(): string {
      if (this._value === '') { this.reload() };// async, so only observers will be updated
      return this._value;
    }
  }

}

makeAutoObservable(serverAPI);

export const serverContext = React.createContext(serverAPI);
