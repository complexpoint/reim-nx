// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.css';
import { serverContext, serverAPI } from './server-context';
import { IpOutput } from './ip-output/ip-output';
import { DevToolsLauncher } from './dev-tools-launcher/dev-tools-launcher';
import { KeyboardConnectionLink } from './keyboard-connection-link/keyboard-connection-link';

export function App() {
  return (
    <serverContext.Provider value={serverAPI}>
      <div className={styles['controls-container']}>
        Controls:
        <IpOutput />
        <DevToolsLauncher />
        <KeyboardConnectionLink />
      </div>
      <div>
        <a
          href="http://localhost:4201"
          target="_blank"
          rel="noreferrer noopener"
        >
          Open tree
        </a>
      </div>
    </serverContext.Provider>
  );
}

export default App;
