import { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { serverContext } from '../server-context';
import styles from './ip-output.module.css';

export const IpOutput = observer(() => {
  const serverAPI = useContext(serverContext);

  return (
    <div className={styles['container']}>
      <span className={styles['ip-box']}>
        IP address: {serverAPI.ipAddress.value}
      </span>
      <button onClick={(e) => serverAPI.ipAddress.reload()}>Reload</button>
    </div>
  );
});
export default IpOutput;
