import { render } from '@testing-library/react';

import IpOutput from './ip-output';

describe('IpOutput', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<IpOutput />);
    expect(baseElement).toBeTruthy();
  });
});
