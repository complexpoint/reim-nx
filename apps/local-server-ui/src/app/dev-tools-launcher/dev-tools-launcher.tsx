import { useContext } from 'react';

import styles from './dev-tools-launcher.module.css';
import { serverContext } from '../server-context';
/* eslint-disable-next-line */
export interface DevToolsLauncherProps {}

//
export function DevToolsLauncher(props: DevToolsLauncherProps) {
  const serverAPI = useContext(serverContext);
  return (
    <div className={styles['container']}>
      Developer Tools:{' '}
      <button
        onClick={(e) => {
          serverAPI.openDevTools();
        }}
      >
        Launch
      </button>
    </div>
  );
}

export default DevToolsLauncher;
