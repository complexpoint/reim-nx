import { render } from '@testing-library/react';

import DevToolsLauncher from './dev-tools-launcher';

describe('DevToolsLauncher', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DevToolsLauncher />);
    expect(baseElement).toBeTruthy();
  });
});
