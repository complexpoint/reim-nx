// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Route, Link, BrowserRouter, Routes, Outlet } from 'react-router-dom';
import styles from './app.module.css';
import Welcome from './welcome/welcome';
import PageLayout from './page-layout/page-layout';
import Demo01 from './demo01/demo01';
import Demo02 from './demo02/demo02';
export function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageLayout />}>
            <Route path="/" element={<Welcome />}></Route>
            <Route path="demo-1" element={<Demo01 />}></Route>
            <Route path="demo-2" element={<Demo02 />}></Route>
          </Route>
        </Routes>
      </BrowserRouter>
      <div />
    </>
  );
}

export default App;
