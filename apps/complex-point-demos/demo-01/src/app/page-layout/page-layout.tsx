import { Outlet, Link, useLocation } from 'react-router-dom';

import styles from './page-layout.module.css';

/* eslint-disable-next-line */
export interface PageLayoutProps {}

export function PageLayout(props: PageLayoutProps) {
  const location = useLocation();
  const HomeLink = location.pathname !== '/' ? <Link to="/">Home</Link> : '';
  return (
    <div className={styles['container']}>
      {HomeLink}
      <Outlet />
    </div>
  );
}

export default PageLayout;
