import { render } from '@testing-library/react';

import Demo01 from './demo01';

describe('Demo01', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Demo01 />);
    expect(baseElement).toBeTruthy();
  });
});
