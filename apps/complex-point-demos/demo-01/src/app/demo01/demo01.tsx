import styles from './demo01.module.css';

/* eslint-disable-next-line */
export interface Demo01Props {}

export function Demo01(props: Demo01Props) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Demo01!</h1>
    </div>
  );
}

export default Demo01;
