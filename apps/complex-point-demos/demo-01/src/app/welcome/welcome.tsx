import { Link } from 'react-router-dom';
import styles from './welcome.module.css';

/* eslint-disable-next-line */
export interface WelcomeProps {}

export function Welcome(props: WelcomeProps) {
  return (
    <>
      {' '}
      <div className={styles['container']}>
        <h1>Welcome to ComplexPoint</h1>
        ComplexPoint is an app for creating beautiful mathematical
        presentations. <br />
        This page contains demo presentations. <br />
        Please note that the presentations are not optimized for small screens
        yet, so to have the best experience possible, view them on your
        computer.
      </div>
      <nav>
        <ul>
          <li>
            <Link to="/demo-1">Demo 1</Link>
          </li>
          <li>
            <Link to="/demo-2">Demo 2</Link>
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Welcome;
