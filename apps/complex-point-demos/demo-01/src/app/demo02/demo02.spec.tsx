import { render } from '@testing-library/react';

import Demo02 from './demo02';

describe('Demo02', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Demo02 />);
    expect(baseElement).toBeTruthy();
  });
});
