import styles from './demo02.module.css';

/* eslint-disable-next-line */
export interface Demo02Props {}

export function Demo02(props: Demo02Props) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Demo02!</h1>
    </div>
  );
}

export default Demo02;
