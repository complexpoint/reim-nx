import { contextBridge, ipcRenderer } from 'electron';

contextBridge.exposeInMainWorld('electron', {
  getAppVersion: () => ipcRenderer.invoke('get-app-version'),
  platform: process.platform,
  getIpAddress: () => ipcRenderer.invoke('get-ip-address'),
  getConnectionAddressForKeyboard: () => ipcRenderer.invoke('get-connection-address-for-keyboard'),
  openDevTools: () => ipcRenderer.invoke('open-dev-tools'),
});
