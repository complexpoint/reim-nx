/**
 * This module is responsible on handling all the inter process communications
 * between the frontend to the electron backend.
 */

import { app, ipcMain } from 'electron';
import { environment } from '../../environments/environment';
import * as ip from 'ip';
import App from '../app';

export default class ElectronEvents {
  static bootstrapElectronEvents(): Electron.IpcMain {
    return ipcMain;
  }
}

// Retrieve app version
ipcMain.handle('get-app-version', () => { // omitted param is event
  console.log(`Fetching application version... [v${environment.version}]`);

  return environment.version;
});

// Retrieve local IP address
ipcMain.handle('get-ip-address', () => { // omitted param is event
  return ip.address();
})

// Get address to connect net keyboard
ipcMain.handle('get-connection-address-for-keyboard', () => {
  return `http://${ip.address()}/keyboard`
})

// Open DevTools
ipcMain.handle('open-dev-tools', () => {
  App.mainWindow?.webContents.openDevTools();
});

// Handle App termination
ipcMain.on('quit', (event, code) => {
  app.exit(code);
});
