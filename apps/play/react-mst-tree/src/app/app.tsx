// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.css';
import { TreeView } from './tree/tree';

import { TreeNodeModel, TreeModel } from '@reim/tree-model';

const root = TreeNodeModel.create({
  selected: true,
  collapsed: false,
  data: 'root',
  properties: {
    data: {
      child1: {
        selected: false,
        collapsed: false,
        data: 'Child 1',
        properties: { data: {} },
      },
      child2: {
        selected: false,
        collapsed: false,
        data: 'Child 2',
        properties: { data: {} },
      },
    },
  },
});

const tree = TreeModel.create({
  root: root,
  focusedNodePath: '/root',
});
export function App() {
  return (
    <>
      <TreeView tree={tree}></TreeView>
      <div />
    </>
  );
}

export default App;
