import styles from './tree.module.css';
import { TreeNode, Tree } from '@reim/tree-model';
import { observer } from 'mobx-react-lite';
import 'bootstrap-icons/font/bootstrap-icons.css';
import classNames from 'classnames';
import { TreeNodesMap } from '@reim/tree-model';
/* eslint-disable-next-line */
export interface TreeProps {
  tree: Tree;
}

export const TreeView = observer(function Tree(props: TreeProps) {
  const tree = props.tree;
  return (
    <div>
      <div>{tree.focusedNodePath}</div>
      <div className={styles['container']}>
        <Node node={props.tree.root}></Node>
      </div>
    </div>
  );
});

export interface NodeProps {
  node: TreeNode;
}
export const Node = observer((props: NodeProps) => {
  const node: TreeNode = props.node;
  const properties: TreeNodesMap = node.properties;
  // properties render

  const renderedProperties = properties
    .entriesArray()
    .map((entry: [string, TreeNode]) => {
      const name = entry[0];
      const node = entry[1] as TreeNode;
      return (
        <div key={entry[0]}>
          <span>
            <PropName name={name} edited={true}></PropName>
          </span>
          <Node node={node}></Node>
        </div>
      );
    });
  return (
    <div className={classNames('treeNode')} onClick={node.setFocus}>
      <span className={classNames(styles['treeNodeHeader'], styles['framed'])}>
        <CollapseControl node={node}></CollapseControl>
        <SelectionControl node={node}></SelectionControl>

        <span className={styles['editButtonsGroup']}>
          <button
            className={styles['editButton']}
            onClick={node.properties.newDefault}
          >
            +
          </button>
          <button className={styles['editButton']} onClick={node.delete}>
            -
          </button>
        </span>

        {node.data}
      </span>
      {node.properties.length() > 0 && !node.collapsed && (
        <div className={styles['propertiesContainer']}>
          <span className={styles['propertiesGutter']}></span>
          {renderedProperties}
        </div>
      )}
    </div>
  );
});

interface PropNameProps {
  name: string;
  edited?: boolean;
}

const PropName = observer((props: PropNameProps) => {
  if (props.edited) {
    return (
      <input
        className={styles['propName']}
        type="text"
        placeholder="selected"
        value={props.name}
      ></input>
    );
  } else {
    return <span className={styles['propName']}>{props.name}:</span>;
  }
});

interface SelectionControlProps {
  node: TreeNode;
}
const SelectionControl = observer((props: SelectionControlProps) => {
  const node = props.node;
  return (
    <input
      title=""
      type="checkbox"
      checked={node.selected}
      placeholder="selected"
      onChange={(e) => node.setSelected(e.currentTarget.checked)}
    ></input>
  );
});

interface CollapseControlProps {
  node: TreeNode;
}

const CollapseControl = observer((props: CollapseControlProps) => {
  const node = props.node;
  const cssClass =
    node.collapsed || !node.hasChildren()
      ? 'bi bi-chevron-right'
      : 'bi bi-chevron-down';

  return (
    <button
      className={classNames(styles['collapseButton'], cssClass)}
      disabled={node.properties.length() === 0}
      onClick={node.toggleCollapsed}
    ></button>
  );
});

export default Tree;
