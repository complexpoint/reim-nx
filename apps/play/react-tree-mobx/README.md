# Generic Tree Editor

Web-based editor for Trees. (WIP)

[Live demo](https://complexpoint.bitbucket.io/react-tree-mobx/)

It consists of the very basic Tree structure that provides only the bare minimum of functionality, and separate Managers, one can add to the Editor in order to perform operations on the Tree.

Auxillary data needed for Managers to perform their operations is not stored in the Nodes, but rather kept external to the Tree, in the Managers themselves.
This holds true even for such data as 'selected' boolean field.

This hopefully allows for high level of configurability, so the same Tree structure could be used in different scenarios throughout the whole Application: from simply presenting trees to providing sophisticated editing capabilities such as multiple selection, custom Node decorations, several simultaneous selections, etc.

In the default configuration, the Editor allows for adding, deleting, selecting, folding Nodes.

![preview!](img/preview.jpg "preview")


