import { render } from '@testing-library/react';

import CommandOptions from './command-options';

describe('CommandOptions', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CommandOptions />);
    expect(baseElement).toBeTruthy();
  });
});
