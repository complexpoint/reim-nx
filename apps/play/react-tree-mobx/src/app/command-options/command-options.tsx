import { observer } from 'mobx-react-lite';
import { Command, BooleanCommandOption } from '../../libs/core/interfaces';
import styles from './command-options.module.css';

/* eslint-disable-next-line */
export interface CommandOptionsProps {
  command?: Command;
}

export const CommandOptions = observer(function CommandOptions(
  props: CommandOptionsProps
) {
  if (!props.command || !props.command.available || !props.command.options)
    return <span></span>;

  const options = Object.getOwnPropertyNames(props.command.options).map(
    (name) => {
      const options = props?.command?.options;
      if (!options) return <span></span>;
      const option = options[name];
      if (!option) {
        return <span></span>;
      }
      let elem = <EmptyOption name={name}></EmptyOption>;
      switch (option.type) {
        case 'boolean':
          elem = (
            <BooleanOption
              name={name}
              commandOption={option as BooleanCommandOption}
            ></BooleanOption>
          );
          break;
      }
      return (
        <label key={name}>
          {name}:&nbsp;{elem}
        </label>
      );
    }
  );

  return (
    <div className={styles['container']} data-preserve-focus>
      <form>{options}</form>
    </div>
  );
});

/* eslint-disable-next-line */
export interface BooleanOptionProps {
  name: string;
  commandOption: BooleanCommandOption;
}
const BooleanOption = observer(function BooleanOption(
  props: BooleanOptionProps
) {
  return (
    <input
      type="checkbox"
      title={props.name}
      value={props.commandOption.value ? 1 : 0}
      onChange={() => (props.commandOption.value = !props.commandOption.value)}
    ></input>
  );
});
/* eslint-disable-next-line */
export interface EmptyOptionProps {
  name: string;
}
function EmptyOption(props: EmptyOptionProps) {
  return <span></span>;
}
export default CommandOptions;
