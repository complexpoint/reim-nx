import { render } from '@testing-library/react';

import CommandButton from './command-button';

describe('CommandButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CommandButton />);
    expect(baseElement).toBeTruthy();
  });
});
