import { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import 'bootstrap-icons/font/bootstrap-icons.css';
import classNames from 'classnames';
import styles from './command-button.module.css';
import { Command } from '../../libs/core/interfaces';
import { EditorContext } from '../tree-editor/tree-editor';
/* eslint-disable-next-line */
export interface CommandButtonProps {
  command: Command;
}

export const CommandButton = observer(function CommandButton(
  props: CommandButtonProps
) {
  const enableOptionsButton = false; // disable for now. TODO: enable options button

  const editor = useContext(EditorContext);
  const disabled = !props.command.available;
  const isCurrent =
    editor.currentCommand.get() === props.command && editor.focus.currentNode; //TODO: test
  // const withOptions = isCurrent;
  return (
    <div data-preserve-focus className={styles['container']}>
      <div className={styles['group']}>
        <button
          data-element-type="CommandButton"
          onClick={(e) => {
            props.command.run();
          }}
          disabled={disabled}
          className={classNames(
            isCurrent ? styles['standout'] : '',
            styles['button']
          )}
        >
          {props.command.name}
        </button>
        {props.command.options && enableOptionsButton ? (
          <button
            disabled={disabled}
            onClick={(e) => {
              if (!editor.currentCommand.get()) {
                editor.currentCommand.set(props.command);
              } else {
                // editor.currentCommand.set(undefined);
              }
            }}
            title="Options"
          >
            <i className="bi bi-gear"></i>
          </button>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );
});

export interface CommandOptionsButtonProps {
  command: Command;
}

export const CommandOptionsButton = observer(function CommandOptionsButton(
  props: CommandOptionsButtonProps
) {
  const editor = useContext(EditorContext);
  return (
    <button
      data-preserve-focus
      onClick={(e) => {
        editor.currentCommand.set(props.command);
      }}
    >
      Options
    </button>
  );
});

export default CommandButton;
