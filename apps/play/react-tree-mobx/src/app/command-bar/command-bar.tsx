import styles from './command-bar.module.css';
import CommandButton from '../command-button/command-button';
import { Editor } from '../../libs/core/interfaces';
// import * as cmd from '../core/commands/commands';

/* eslint-disable-next-line */
export interface CommandBarProps {
  editor: Editor;
}

export function CommandBar(props: CommandBarProps) {
  const commands = Array.from(props.editor.commands.values());
  const buttons = commands.map((command) => {
    return <CommandButton key={command.name} command={command}></CommandButton>;
  });
  return <div className={styles['container']}>{buttons}</div>;
}

export default CommandBar;
