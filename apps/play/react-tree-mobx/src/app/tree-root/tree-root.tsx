import { observer } from 'mobx-react-lite';
import styles from './tree-root.module.css';
import { Tree } from '../../libs/core/interfaces';
import { NodeList } from '../node-list/node-list';

/* eslint-disable-next-line */
export interface TreeRootProps {
  tree: Tree;
}

export const TreeRoot = observer(function TreeRoot(props: TreeRootProps) {
  return (
    <div className={styles['container']}>
      <NodeList nodes={props.tree.children} offset={false}></NodeList>
    </div>
  );
});

export default TreeRoot;
