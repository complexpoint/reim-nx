import { render } from '@testing-library/react';

import TreeRoot from './tree-root';

describe('TreeRoot', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TreeRoot />);
    expect(baseElement).toBeTruthy();
  });
});
