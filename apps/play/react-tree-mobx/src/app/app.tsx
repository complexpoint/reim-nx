// eslint-disable-next-line @typescript-eslint/no-unused-vars

import { Tree, Node } from '../libs/default-implementation/tree';
import { DefaultTreeEditor } from '../libs/default-implementation/editor';
import TreeEditor, { clickedOutsideEditor } from './tree-editor/tree-editor';

const tree = new Tree([
  new Node(),
  new Node([new Node([new Node()]), new Node()]),
  new Node(),
]);

const editor = new DefaultTreeEditor(tree);
// deselect current node when clicked outside the editor
window.document.addEventListener('click', (e) => {
  if (clickedOutsideEditor(e.target)) {
    editor.focus.currentNode = undefined;
  }
});
export function App() {
  return <TreeEditor editor={editor}></TreeEditor>;
}

export default App;
