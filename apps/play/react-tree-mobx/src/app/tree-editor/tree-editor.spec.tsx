import { render } from '@testing-library/react';
import { Tree, Node } from '../../libs/default-implementation/tree';
import { DefaultTreeEditor } from '../../libs/default-implementation/editor';
import TreeEditor from './tree-editor';

const tree = new Tree([
  new Node(),
  new Node([new Node([new Node()]), new Node()]),
  new Node(),
]);
const editor = new DefaultTreeEditor(tree);
describe('TreeEditor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TreeEditor editor={editor} />);
    expect(baseElement).toBeTruthy();
  });
});
