import { observer } from 'mobx-react-lite';
import React, { useRef } from 'react';
import styles from './tree-editor.module.css';
import TreeRoot from '../tree-root/tree-root';
import { Tree } from '../../libs/default-implementation/tree';
import CommandBar from '../command-bar/command-bar';
import { CommandOptions } from '../command-options/command-options';
import { DefaultTreeEditor } from '../../libs/default-implementation/editor';
import {
  StandardEditor,
  FocusManager,
  CollapseManager,
} from '../../libs/core/interfaces';

const tree = new Tree([]);
const editor = new DefaultTreeEditor(tree);
export const FocusContext = React.createContext<FocusManager | undefined>(
  undefined
);
export const CollapseContext = React.createContext<CollapseManager | undefined>(
  undefined
);
export const EditorContext = React.createContext<StandardEditor>(editor);

/* eslint-disable-next-line */
export interface TreeEditorProps {
  editor: StandardEditor;
}

export const TreeEditor = observer(function TreeEditor(props: TreeEditorProps) {
  const editor = props.editor;
  const tree = editor.tree;
  const mainDiv = useRef<HTMLDivElement>(null);
  const RenderedTree = tree ? <TreeRoot tree={tree}></TreeRoot> : <div></div>;

  return (
    <div
      data-editor-id={editor.id}
      ref={mainDiv}
      tabIndex={0}
      onKeyUp={(e) => {
        processKey(e, props.editor);
      }}
      className={styles['container']}
      onClick={(e) => {
        if (blurFocusOnClick(e.target)) {
          editor.focus.currentNode = undefined;
        }
        mainDiv.current?.focus(); // TODO: focus() does not work
      }}
      // onBlur={(e) => editor.focus.deselect()}
    >
      <EditorContext.Provider value={editor}>
        <FocusContext.Provider value={props.editor.focus}>
          <CollapseContext.Provider value={props.editor.collapsedNodes}>
            <CommandBar editor={editor}></CommandBar>
            <CommandOptions
              command={editor.currentCommand.get()}
            ></CommandOptions>
            {RenderedTree}
          </CollapseContext.Provider>
        </FocusContext.Provider>
      </EditorContext.Provider>
    </div>
  );
});

function processKey(e: React.KeyboardEvent, editor: StandardEditor) {
  switch (e.code) {
    case 'ArrowUp':
      editor.nav?.selectPrevNode(editor.focus, editor.collapsedNodes);
      break;
    case 'ArrowDown':
      editor.nav?.selectNextNode(editor.focus, editor.collapsedNodes);
      break;
  }
}

function findInTargets(target: EventTarget | null, dataName: string): boolean {
  if (!target) return false;
  let current = target as HTMLElement;
  while (current) {
    if (current?.dataset[dataName] !== undefined) return true;
    if (!current.parentElement) return false;
    current = current.parentElement;
  }
  return false;
}

export function blurFocusOnClick(target: EventTarget | null): boolean {
  return !findInTargets(target, 'preserveFocus');
}

export function clickedOutsideEditor(target: EventTarget | null): boolean {
  return !findInTargets(target, 'editorId');
}

export default TreeEditor;
