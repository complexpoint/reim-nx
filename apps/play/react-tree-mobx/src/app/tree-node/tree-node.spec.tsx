import { render } from '@testing-library/react';
import { Node } from '../../libs/default-implementation/tree';
import TreeNode from './tree-node';

describe('TreeNode', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TreeNode node={new Node()} />);
    expect(baseElement).toBeTruthy();
  });
});
