import 'bootstrap-icons/font/bootstrap-icons.css';
import { useContext, createContext } from 'react';
import { observer } from 'mobx-react-lite';
import styles from './tree-node.module.css';
import { Node } from '../../libs/core/interfaces';
import NodeList from '../node-list/node-list';
import { FocusContext, CollapseContext } from '../tree-editor/tree-editor';

export interface TreeNodeProps {
  node: Node;
}

export const TreeNode = observer(function TreeNode(props: TreeNodeProps) {
  const node = props.node;
  const focusCtx = useContext(FocusContext);
  const isFocused = focusCtx?.currentNode?.id === node.id;
  const isCollapsed = useContext(CollapseContext)?.isCollapsed(node);

  return (
    <div>
      <NodeContext.Provider value={node}>
        <div
          className={
            styles['tree-node'] + `${isFocused ? ' ' + styles['focused'] : ''}`
          }
          data-element-type="Node"
          data-preserve-focus
          onClick={(e) => {
            if (focusCtx) {
              focusCtx.currentNode = node;
            }
          }}
        >
          <CollapseControl></CollapseControl>
          {node.text}
        </div>
      </NodeContext.Provider>
      <div id="children">
        {isCollapsed ? <span /> : <NodeList nodes={node.children}></NodeList>}
      </div>
    </div>
  );
});

/* eslint-disable-next-line */
export interface CollapseControlProps {}
export const CollapseControl = observer(function CollapseControl(
  props: CollapseControlProps
) {
  const node = useContext(NodeContext);
  const collapsedCtx = useContext(CollapseContext);
  if (!node || !collapsedCtx) return <span />;

  const isCollapsed = collapsedCtx ? collapsedCtx.has(node) : false;

  return node.children.length > 0 ? (
    <button
      title={isCollapsed ? 'show children' : 'hide children'}
      className={styles['collapseButton']}
      onMouseUp={(e) => {
        if (isCollapsed) {
          collapsedCtx.remove(node);
        } else {
          collapsedCtx.add(node);
        }
      }}
    >
      <i
        className={isCollapsed ? 'bi bi-chevron-right' : 'bi bi-chevron-down'}
      ></i>
    </button>
  ) : (
    <span />
  );
});

export const NodeContext = createContext<Node | undefined>(undefined);

export default TreeNode;
