import { observer } from 'mobx-react-lite';
import styles from './node-list.module.css';
import { NodeList as Model } from '../../libs/core/interfaces';
import TreeNode from '../tree-node/tree-node';
/* eslint-disable-next-line */
export interface NodeListProps {
  nodes: Model;
  offset?: boolean;
}

export const NodeList = observer(function NodeList(props: NodeListProps) {
  const nodes = props.nodes.array().map((item) => {
    return <TreeNode key={item.id} node={item}></TreeNode>;
  });
  const offsetCssClass = props.offset === undefined ? 'offset' : 'no-offset';

  return <div className={styles[offsetCssClass]}>{nodes}</div>;
});
export default NodeList;
