import { ID } from './id'

//----------------
// Tree
//----------------

interface TreeMember {
  readonly tree?: Tree;
  readonly children?: NodeList;
  readonly level: number;
}

export interface Tree extends TreeMember {
  readonly type: "tree";
  readonly children: NodeList;
}

export type NodeParent = Node | Tree;

export interface Node extends TreeMember {
  readonly type: "node";
  readonly id: NodeID;
  readonly children: NodeList;
  text: string;
  parent?: NodeParent;
  nodeList?: NodeList;
}

export type NodeID = ID; // TODO: make opaque class

export type NodeListHost = Node | Tree;

export interface NodeList<N extends Node = Node> {
  add: (node: N, position?: number) => void;
  getById: (id: NodeID) => Node | undefined;
  getByPosition: (position: number) => Node | undefined;
  readonly first: Node | undefined;
  readonly last: Node | undefined;
  getPosition(id: NodeID): number | undefined;
  remove: (id: NodeID) => N | undefined;
  has: (id: NodeID) => boolean;
  array: () => N[]; // TODO: make NodeList be an Array subclass
  host?: NodeListHost;
  readonly length: number;
}

//------------------------
// Manager
//--------------------------

export interface Manager {
  editor: Editor;
}

export type ManagerName = string;

export interface FocusManager extends Manager {
  currentNode?: Node;
  readonly tree?: Tree;
}

export interface NavigationManger extends Manager {
  selectNextNode(focus: FocusManager, collapsed: NodeBooleanPropertyManager): void;
  selectPrevNode(focus: FocusManager, collapsed: NodeBooleanPropertyManager): void;
}

export interface EditingManager extends Manager {
  addNodeBefore: (focus: FocusManager) => Node | undefined;
  addNodeAfter: (focus: FocusManager) => void;
  addChildNode: (focus: FocusManager) => void;
  deleteNode: (focus: FocusManager) => void;
}

export interface NodeDataManager<D> extends Manager {
  get(node: Node): D;
  set(node: Node, data: D): void;
}

export interface NodeBooleanPropertyManager extends Manager {
  add(node?: Node): void;
  remove(node?: Node): void;
  has(node?: Node): boolean;
}

export interface CollapseManager extends NodeBooleanPropertyManager {
  isCollapsed(node: Node): boolean;
}

export interface CurrentCommandManager extends Manager {
  get(): Command | undefined;
  set(value?: Command): void;
}

//-----------------------
// Editor
//----------------------
export type EditorID = ID;

export interface Editor {
  readonly id: EditorID;
  tree?: Tree;
  readonly managers: Map<ManagerName, Manager>;
  readonly commands: Map<CommandName, Command>;
}

export interface StandardEditor extends Editor {
  readonly focus: FocusManager,
  readonly nav: NavigationManger,
  readonly edit: EditingManager,
  readonly collapsedNodes: CollapseManager,
  readonly permanentNodes: NodeBooleanPropertyManager,
  readonly currentCommand: CurrentCommandManager;
}

//--------------------------
// Command
//--------------------------

export type CmdFn<Options extends CommandOptions = Record<string, CommandOption>, Result = void> = (options?: Options) => Result;
export type CommandName = string;

export interface Command<Options extends CommandOptions = Record<string, CommandOption>, Result = void> {
  readonly name: CommandName;
  readonly run: CmdFn<Options, Result>;
  readonly available: boolean;
  readonly options?: CommandOptions;
}

// TODO: change to `string` to male it open list
export type CommandOptionType = "boolean" | "number" | "string";

// TODO: not type safe
export interface CommandOption {
  type: CommandOptionType;
  value: boolean | number | string;
}

export interface BooleanCommandOption extends CommandOption {
  type: 'boolean';
  value: boolean
}

export type CommandOptions = Record<string, CommandOption>


