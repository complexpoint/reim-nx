import { v4 } from "uuid";

export type ID = string;

export function generate(): ID {
  return v4()
}
