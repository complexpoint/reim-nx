import * as uuid from 'uuid';
import { makeAutoObservable } from 'mobx';
import * as I from '../core/interfaces';

export class NodeList implements I.NodeList {

  readonly _nodes = new Array<I.Node>();
  readonly _idIndex = new Map<I.NodeID, I.Node>();
  _host?: I.NodeListHost = undefined;

  constructor(nodes?: I.Node[], host?: I.NodeListHost) {
    this.host = host; // set before adding nodes!
    if (nodes) {
      for (const node of nodes) {
        this.add(node);
      }
    }
    makeAutoObservable(this);
  }
  getById(id: I.NodeID): I.Node | undefined {
    return this._idIndex.get(id);
  }
  getByPosition(position: number): I.Node | undefined {
    const length = this.length;
    if (Math.abs(position) > length) return undefined;
    return this._nodes[position < 0 ? length + position : position];
  }
  array() {
    return this._nodes;
  }
  add(node: I.Node, position?: number): void {
    if (this._idIndex.has(node.id)) {
      throw new Error(`NodeList already has Node with ID '${node.id}'`)
    }
    node.nodeList = this;
    node.parent = this.host;
    this._idIndex.set(node.id, node);
    if (position === undefined) {
      this._nodes.push(node);
    } else {
      if (position < 0 || position > this.length) {
        throw new Error(`position ${position} is out of bounds`)
      } else {
        this._nodes.splice(position, 0, node);
      }
    }
  }

  remove(id: string): I.Node | undefined {
    const node = this._idIndex.get(id);
    if (node === undefined) return;
    this._idIndex.delete(id); // TODO: do not throw away return value
    this._nodes.splice(this._nodes.indexOf(node), 1);
    return node;
  }
  has(id: string): boolean {
    return this._idIndex.has(id)
  }
  get last(): I.Node | undefined {
    return this.getByPosition(this.length - 1);
  }
  get first(): I.Node | undefined {
    return this.getByPosition(0)
  }
  get host() {
    return this._host;
  }
  set host(value: I.NodeListHost | undefined) {
    this._host = value;
  }
  get length(): number {
    return this._nodes.length;
  }
  getPosition(id: I.NodeID): number | undefined {
    const node = this._idIndex.get(id);
    if (node === undefined) return undefined;
    return this._nodes.indexOf(node);
  }
  flatArray(): I.Node[] { // not from interface
    let result: I.Node[] = [];
    for (const node of this._nodes) {
      result.push(node);
      result = result.concat(node.children.array());
    }
    return result;
  }
}

export class Tree implements I.Tree {
  readonly type: 'tree' = 'tree';
  readonly children: NodeList;
  _currentNode?: I.Node = undefined;

  constructor(nodes?: I.Node[]) {
    this.children = new NodeList(nodes, this);
    makeAutoObservable(this);
  }
  get level(): number {
    return 0;
  }
  get currentNode() {
    return this._currentNode;
  }
  set currentNode(value) {
    this._currentNode = value;
  }
  get tree(): I.Tree {
    return this;
  }
}

export class Node implements I.Node {
  id: string;
  children: I.NodeList<I.Node>;
  text: string;
  _parent?: I.NodeParent;
  _list?: I.NodeList;




  constructor(children?: I.Node[]) {
    this.id = uuid.v4();
    this.children = new NodeList(children, this);
    this.text = this.id;
    makeAutoObservable(this);
  }
  get type(): 'node' {
    return 'node'
  }
  get level(): number {
    return this.parent ? this.parent.level + 1 : 0;
  }
  get parent(): I.NodeParent | undefined {
    return this._parent
  }
  set parent(value: I.NodeParent | undefined) {
    this._parent = value;
  }
  get tree(): I.Tree | undefined {
    return this.parent?.tree
  }
  get nodeList() {
    return this._list;
  }
  set nodeList(value: I.NodeList | undefined) {
    this._list = value;
  }
  get position(): number {
    if (!this.nodeList) throw new Error(`TreeNode.position called on detached TreeNode`)
    const pos = this.nodeList.getPosition(this.id);
    if (pos === undefined) throw new Error(`node ${this.id} does not have a position in its own group`);
    return pos;
  }

  toString(): string {
    return this.id;
  }
}




