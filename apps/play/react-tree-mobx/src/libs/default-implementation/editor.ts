import { generate as generateId } from '../core/id'
import { DefaultFocusManager } from './managers/focus';
import { Tree, StandardEditor, Manager, FocusManager, EditingManager, NavigationManger, ManagerName, Command, CommandName, CmdFn, CommandOptions, CommandOption } from '../core/interfaces';
import { DefaultNavigationManager } from "./managers/navigation";
import { DefaultEditingManager } from './managers/editing';
import { makeAutoObservable } from "mobx";
import { DefaultNodeBooleanPropertyManager } from './managers/boolean-property';
import { DefaultCollapseManager } from './managers/collapsing';
import { DefaultCurrentCommandManager } from './managers/current-command';

export class DefaultTreeEditor implements StandardEditor {
  private _tree?: Tree;

  readonly id = generateId();

  public managers: Map<ManagerName, Manager> = new Map();
  // TODO: make the props part of Editor interface.
  readonly focus: FocusManager = new DefaultFocusManager(this);
  readonly nav: NavigationManger = new DefaultNavigationManager(this);
  readonly edit: EditingManager = new DefaultEditingManager(this);

  readonly collapsedNodes = new DefaultCollapseManager(this)
  readonly permanentNodes = new DefaultNodeBooleanPropertyManager(this)

  commands: Map<CommandName, Command> = new Map();
  readonly currentCommand = new DefaultCurrentCommandManager(this);

  constructor(tree: Tree | undefined,) {
    this._tree = tree;
    this.managers.set('focus', this.focus);
    this.managers.set('navigation', this.nav);
    this.managers.set('edit', this.edit);
    this.managers.set('collapsed', this.collapsedNodes);
    this.managers.set('permanentNodes', this.permanentNodes);


    makeAutoObservable(this);

    // Commands

    //------------------
    // navigation
    //-------------------
    this.addCommand({
      name: 'select-next',
      run: () => this.nav.selectNextNode(this.focus, this.collapsedNodes),
      available: () => this.tree !== undefined && this.tree?.children.length > 0,
    })

    this.addCommand({
      name: 'select-prev',
      run: () => this.nav.selectPrevNode(this.focus, this.collapsedNodes),
      available: () => this.tree !== undefined && this.tree?.children.length > 0,
    })

    this.addCommand({
      name: 'deselect',
      run: () => this.focus.currentNode = undefined,
      available: () => {
        return this.focus.currentNode !== undefined
      },
    })
    //---------
    // editing
    //---------
    this.addCommand({
      name: 'add-node-before',
      run: (options) => {
        const node = this.edit.addNodeBefore(this.focus);
        if (options && options['permanent']['value']) {
          this.permanentNodes.add(node);
        }
      },
      available: () => this.focus.currentNode !== undefined,
      options: { 'permanent': new DefaultBooleanCommandOption() },
    })

    this.addCommand({
      name: 'add-node-after',
      run: () => this.edit.addNodeAfter(this.focus),
      available: () => this.focus.currentNode !== undefined,
    })

    this.addCommand({
      name: 'add-child-node',
      run: () => this.edit.addChildNode(this.focus),
      available: () => this.focus.tree !== undefined,
    })

    this.addCommand({
      name: 'delete-node',
      run: () => this.edit.deleteNode(this.focus),
      available: () => this.focus.currentNode !== undefined && !this.permanentNodes.has(this.focus.currentNode),
    })

  }

  addCommand(cmd: {
    name: CommandName,
    run: CmdFn,
    available: () => boolean,
    options?: CommandOptions,
  }) {
    this.commands.set(cmd.name, new DefaultCommand(cmd.name, cmd.run, cmd.available, cmd.options));
  }

  get tree(): Tree | undefined {
    return this._tree;
  }

  set tree(value: Tree | undefined) {
    this._tree = value;
    this.focus.currentNode = undefined;  // TODO: test
  }
}

class DefaultCommand implements Command {
  readonly run: CmdFn;
  constructor(
    readonly name: CommandName,
    run: CmdFn,
    readonly availableFn: () => boolean,
    readonly options?: CommandOptions,
  ) {
    this.run = () => run(this.options);
    makeAutoObservable(this)
  }

  get available(): boolean {
    return this.availableFn();
  };
}

class DefaultBooleanCommandOption implements CommandOption {
  type = 'boolean' as const;
  private _value = false;
  get value() { return this._value };
  set value(value) {
    this._value = value;
  }
  constructor(value = false) {
    this._value = value;
    makeAutoObservable(this)
  }

}
