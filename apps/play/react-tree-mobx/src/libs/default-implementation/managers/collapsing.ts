import { CollapseManager, Editor, Node } from "../../core/interfaces";
import { TreeManager } from "../manager";
import { DefaultNodeBooleanPropertyManager } from "./boolean-property";

export class DefaultCollapseManager extends TreeManager implements CollapseManager {
  private readonly boolPropManager: DefaultNodeBooleanPropertyManager;
  constructor(editor: Editor,) {
    super(editor);
    this.boolPropManager = new DefaultNodeBooleanPropertyManager(editor);

  }
  isCollapsed(node: Node): boolean {
    return this.has(node);
  }

  // other methods are delegated

  add(node: Node): void {
    return this.boolPropManager.add(node);
  }
  remove(node: Node): void {
    return this.boolPropManager.remove(node);
  }
  has(node: Node): boolean {
    return this.boolPropManager.has(node);
  }

}
