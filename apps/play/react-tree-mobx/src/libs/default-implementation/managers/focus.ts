import { Node, Tree, FocusManager, Editor, } from '../../core/interfaces';
import { makeAutoObservable } from 'mobx';
import { TreeManager } from '../manager';

export class DefaultFocusManager implements FocusManager, TreeManager {

  private _currentNode?: Node;
  // TODO: make inheritance work. Inherit from TreeManager
  private _treeManager: TreeManager;

  constructor(editor: Editor) {
    this._treeManager = new TreeManager(editor);
    makeAutoObservable(this);
  }

  get editor(): Editor {
    return this._treeManager.editor;
  }

  get currentNode(): Node | undefined {
    return this._currentNode;
  }
  set currentNode(value: Node | undefined) {
    if (value !== undefined && value?.tree !== this.editor.tree) throw new Error(`can't focus on a node not from the tree. Node: ${value}, Tree: ${this.editor.tree}`)
    this._currentNode = value;
  }

  get tree(): Tree | undefined {
    return this.editor.tree
  }

  deselect() {
    this.currentNode = undefined;
  }
}
