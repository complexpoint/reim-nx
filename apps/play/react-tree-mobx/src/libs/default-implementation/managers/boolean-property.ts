import { makeAutoObservable } from 'mobx';
import { Editor, Node, NodeBooleanPropertyManager } from '../../core/interfaces';

export class DefaultNodeBooleanPropertyManager implements NodeBooleanPropertyManager {
  private data: WeakSet<Node> = new Set();

  constructor(readonly editor: Editor) {
    makeAutoObservable(this);
  }
  has(node?: Node): boolean {
    if (!node) return false;
    return this.data.has(node);
  }
  add(node?: Node): void {
    if (!node) return;
    this.data.add(node);
  }
  remove(node?: Node): void {
    if (!node) return;
    this.data.delete(node);
  }

}
