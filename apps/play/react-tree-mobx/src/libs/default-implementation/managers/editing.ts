import { TreeManager } from "../manager";
import { Node, FocusManager, EditingManager, NodeList } from '../../core/interfaces';
import { Node as DefaultNode } from '../tree';
import { } from './focus';
import { position } from './navigation';

export type AddPosition = 'before' | 'after';

export class DefaultEditingManager extends TreeManager implements EditingManager {

  addNodeBefore(focus: FocusManager) {
    const newNode = new DefaultNode();
    addNode(focus, newNode, 'before'); return newNode;
  }
  addNodeAfter(focus: FocusManager) {
    addNode(focus, new DefaultNode(), 'after');
  }
  addChildNode(focus: FocusManager) {
    addChildNode(focus, new DefaultNode(), false);
  }

  deleteNode = deleteNode;

}

export function addNode(focus: FocusManager, node: Node, addPosition: AddPosition,) {
  const newNode = node;
  const currentNode = focus.currentNode;
  if (currentNode === undefined) return;
  const parent = currentNode?.parent;

  if (!parent) {
    const tree = focus.tree;
    if (tree === undefined) return;
    const pos = addPosition === 'after' ? tree.children.length : 0;
    tree.children.add(newNode, pos)
  } else {
    const pos = position(currentNode);
    parent.children.add(newNode, addPosition === 'after' ? pos + 1 : pos);
  }
  focus.currentNode = newNode;
}

export function addChildNode(focus: FocusManager, node: Node, select = false) {

  const parentNode = focus.currentNode || focus.tree;
  if (parentNode === undefined) return;
  const child = node;
  parentNode.children.add(child);
  // multiple sequential invocations should add nodes to the same parent, not to build parent-child chain
  // hence the default select = false
  if (select) focus.currentNode = child;
}

export function deleteNode(focus: FocusManager) {
  if (focus?.currentNode === undefined) {
    return
  } else {
    let pos = position(focus.currentNode);
    // position() throws if Node.nodeList is undefined
    const nodeList = focus.currentNode.nodeList as NodeList;

    nodeList.remove(focus.currentNode.id);
    focus.currentNode = undefined;

    // if deleted node was the only node, select nothing
    if (nodeList.length !== 0) {
      // if deleted node was the last node, select the new last node
      pos = nodeList.length === pos ? pos - 1 : pos;
      focus.currentNode = nodeList.getByPosition(pos);
    }

  }


};

