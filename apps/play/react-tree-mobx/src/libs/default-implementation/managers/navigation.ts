import { TreeManager } from "../manager";
import { Node, NodeParent, FocusManager, NavigationManger, NodeBooleanPropertyManager } from '../../core/interfaces';


export class DefaultNavigationManager extends TreeManager implements NavigationManger {

  selectNextNode(focus: FocusManager, collapsed: NodeBooleanPropertyManager) {

    if (focus.tree === undefined) return;
    if (focus.currentNode === undefined) {
      focus.currentNode = focus.tree.children.getByPosition(0);  // wrap around and start from the top of the tree
    } else {
      focus.currentNode = next(focus.currentNode, 'depth', (node) => collapsed.has(node));
    }
  }
  selectPrevNode(focus: FocusManager, collapsed: NodeBooleanPropertyManager) {
    if (focus.tree === undefined) return;
    if (focus.currentNode === undefined) {
      focus.currentNode = lastChild(focus.tree); // wrap around and start from the bottom of the tree
    } else {
      focus.currentNode = prev(focus.currentNode, (node) => collapsed.has(node));
    };
  }
}

export function position(node: Node): number {
  if (!node.nodeList) throw new Error(`position() called on detached Node`)
  const pos = node.nodeList.getPosition(node.id);
  if (pos === undefined) throw new Error(`node ${node.id} does not have a position in its own group`);
  return pos;
}

type TraverseDirection = 'width' | 'depth';

export function next(node: Node, direction: TraverseDirection, isCollapsed: (node: Node) => boolean): Node | undefined {
  if (!node.nodeList) return undefined;
  const pos = position(node);

  switch (direction) {
    case 'depth':
      if (!isCollapsed(node) && node.children.length > 0) { return node.children.getByPosition(0) }
      else {
        return next(node, 'width', isCollapsed);
      };
    case 'width':
      if (pos < node.nodeList.length - 1) {
        return node.nodeList.getByPosition(pos + 1);
      } else {
        // now we know the node is the last child
        const parent = node.parent;
        if (!parent) return undefined;
        switch (parent.type) {
          case 'tree': {
            return undefined;
          }
          case 'node': {
            return next(parent, 'width', isCollapsed);
          }
        }
      };
  }
}

export function prev(node: Node, isCollapsed: (node: Node) => boolean): Node | undefined {
  if (!node.nodeList) return undefined;
  const pos = position(node);
  if (pos > 0) {
    const prev = node.nodeList.getByPosition(pos - 1);
    if (prev === undefined) throw new Error(`nodeList has node at position ${pos} but doesn't at position ${pos - 1}. Node: ${node.id}`)
    return isCollapsed(prev) ? prev : lastChild(prev);
  }
  // now we know the node is the first child
  const parent = node.parent;
  if (!parent) return undefined;
  switch (parent.type) {
    case 'tree': {
      return undefined;
    }
    case 'node': {
      return parent;
    }
  }
}

export function lastChild(node: NodeParent): Node {
  const last = node.children.last;
  if (last === undefined) { return node as Node } // TODO: may be a bug
  else {
    return lastChild(last);
  };
}
