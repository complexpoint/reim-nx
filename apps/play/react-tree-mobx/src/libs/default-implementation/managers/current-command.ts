import { Command, Tree, CurrentCommandManager, Editor, } from '../../core/interfaces';
import { makeAutoObservable } from 'mobx';
import { TreeManager } from '../manager';

export class DefaultCurrentCommandManager implements CurrentCommandManager, TreeManager {

  private _currentCommand?: Command;
  // TODO: make inheritance work. Inherit from TreeManager
  private _treeManager: TreeManager;

  constructor(editor: Editor) {
    this._treeManager = new TreeManager(editor);
    makeAutoObservable(this);
  }

  get editor(): Editor {
    return this._treeManager.editor;
  }

  get(): Command | undefined {
    return this._currentCommand;
  }
  set(value: Command | undefined) {
    this._currentCommand = value;
  }

  get tree(): Tree | undefined {
    return this.editor.tree
  }

}
