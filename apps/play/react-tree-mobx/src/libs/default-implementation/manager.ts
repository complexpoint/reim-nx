import { makeObservable, observable } from "mobx";
import { Manager, Editor } from "../core/interfaces";

export class TreeManager implements Manager {

  constructor(readonly editor: Editor) {
    makeObservable(this, { editor: observable });
  }

}
