import React from 'react';
import './app.module.css';
import TreeRoot from './tree/tree-root/tree-root';
import {
  defaultNodeFactory,
  initialItems,
} from './node-types-registry/default-node-factory';
import { SelectionMode } from './tree/selection-mode/selection-mode';
import { defaultTreeNodeIdProvider } from './tree/tree-node-id/default-id-provider';
export class App extends React.Component {
  override render(): React.ReactNode {
    return (
      <div>
        <TreeRoot
          items={initialItems}
          treeContext={{
            nodeIdProvider: defaultTreeNodeIdProvider,
            nodeFactory: defaultNodeFactory,
            selectionMode: SelectionMode.Single,
          }}
        />
      </div>
    );
  }
}

export default App;
