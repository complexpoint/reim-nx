import { JSXElementConstructor } from "react";

import { TreeNodeData } from "./tree-node-data";
import { TreeNodeId } from '../tree/tree-node-id/tree-node-id';


export type ReactComponentType = JSXElementConstructor<Record<string, unknown>>;
// export type ReactComponentType = JSXElementConstructor<unknown>;

class NodeTypeDefinition {
  constructor(readonly name: NodeTypeName, readonly nodeClass: ReactComponentType) { };
}
export class NodeTypeName {
  constructor(private name: string) { }
  toString(): string {
    return this.name;
  }
};


export class NodeTypesRegistry {

  readonly types: Map<string, NodeTypeDefinition> = new Map();

  register(name: string, cls: ReactComponentType) {
    const typeName = new NodeTypeName(name);
    this.types.set(typeName.toString(), new NodeTypeDefinition(typeName, cls));
  }

  get(name: NodeTypeName): NodeTypeDefinition | undefined {
    return this.types.get(name.toString());
  }

}
export interface ComponentData {
  Comp: ReactComponentType,
  id: TreeNodeId,
  data: Record<string, unknown>,
  children?: ComponentData[]
}

export type NodeFactory = {
  getComponentFromNodeData: (data: TreeNodeData) => ComponentData;
}

