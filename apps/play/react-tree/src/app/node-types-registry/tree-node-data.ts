import { TreeNodeId } from "../tree/tree-node-id/tree-node-id";
import { NodeTypeName } from "./node-types-registry";

export type NodeDataBaseType = Record<string, unknown>;

export interface TreeNodeData<PropsType extends NodeDataBaseType = NodeDataBaseType> {
  typeName: NodeTypeName;
  id: TreeNodeId;
  data: PropsType;
  children?: TreeNodeData[];
}

export interface NodeDataPlainObject {
  typeName: string;
  id: string;
  data: Record<string, unknown>;
  children?: NodeDataPlainObject[];

}

export function nodeDataFromPlainObject(item: NodeDataPlainObject): TreeNodeData {
  return {
    typeName: new NodeTypeName(item.typeName),
    id: new TreeNodeId(item.id),
    data: item.data,
    children: item.children ? item.children.map(nodeDataFromPlainObject) : undefined
  };
}
