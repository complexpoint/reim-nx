import React from 'react';
import { NodeFactory, NodeTypesRegistry, NodeTypeName, ReactComponentType, ComponentData } from './node-types-registry';
import { TreeNodeData, NodeDataPlainObject, nodeDataFromPlainObject } from './tree-node-data';


import { NodeError, ErrorNodeType } from '../nodes/nodes';

import * as nodes from '../nodes/nodes';

export class BasicNodeFactory implements NodeFactory {

  constructor(readonly nodeTypes: NodeTypesRegistry, readonly errorNode: ErrorNodeType) { }

  getComponentFromNodeData(def: TreeNodeData): ComponentData {
    const compDef = this.nodeTypes.get(def.typeName) || {
      name: new NodeTypeName('Error'),
      id: def.id,
      nodeClass: this.errorNode,
    };
    if (compDef.name.toString() === 'Error') {
      def = {
        typeName: new NodeTypeName('NodeError'),
        id: def.id,
        data: { msg: `unknown node type "${def.typeName}"` },
      };
    }
    return { Comp: compDef.nodeClass as ReactComponentType, id: def.id, data: def.data };
  }

}

// register node types
const nodeTypes = new NodeTypesRegistry();
nodeTypes.register('TextLine', nodes.NodeTextLine);
nodeTypes.register('NumericCounter', nodes.NodeNumericCounter as unknown as ReactComponentType);
nodeTypes.register('Group', nodes.NodeGroup);

// TODO: move to tests
const initialItemsPlainData: Array<NodeDataPlainObject> = [
  { typeName: 'TextLine', id: '1', data: { text: 'line 1 text' } },
  { typeName: 'TextLine', id: '2', data: { text: 'Hello line 2 text' } },
  { typeName: 'NumericCounter', id: '3', data: { title: 'Counter 1' } },
  { typeName: 'NodeNoSuchNode', id: '4', data: {} }, // to test error
  {
    typeName: 'Group', id: '5', data: { name: 'Group1' }, children: [
      { typeName: 'TextLine', id: '1', data: { text: 'line 1 text' } },
      {
        typeName: 'Group', id: '6', data: { name: 'Group1' }, children: [
          { typeName: 'TextLine', id: '1', data: { text: 'line 1 text' } },
        ]
      },
      { typeName: 'TextLine', id: '2', data: { text: 'Hello line 2 text' } },
    ]
  }
]
// TODO: move to tests
// generate initial tree state
export const initialItems: Array<TreeNodeData> = initialItemsPlainData.map(nodeDataFromPlainObject);


export const defaultNodeFactory = new BasicNodeFactory(nodeTypes, NodeError);
// TODO: separate from default-node-factory?
export const NodeFactoryContext = React.createContext<NodeFactory>(defaultNodeFactory);
