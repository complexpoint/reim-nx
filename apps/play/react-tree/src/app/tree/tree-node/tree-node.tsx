import { Component } from 'react';

import { TreeNodeId } from '../tree-node-id/tree-node-id';
import {
  NodeTypeName,
  ReactComponentType,
  NodeFactory,
} from '../../node-types-registry/node-types-registry';
import {
  NodeDataBaseType,
  TreeNodeData,
} from '../../node-types-registry/tree-node-data';
import styles from './tree-node.module.css';
import { TreeContext } from '../tree-context/tree-context';
import { SelectionMode } from '../selection-mode/selection-mode';

/* eslint-disable-next-line */
export interface TreeNodeProps {
  id: TreeNodeId;
  component: ReactComponentType;
  data: NodeDataBaseType;
  children?: TreeNodeData<NodeDataBaseType>[];
  treeContext: TreeContext;
  nodeGroup: NodeGroup;
  selected: boolean;
}

export interface TreeNodeState {
  collapsed: boolean;
}

export class TreeNode extends Component<TreeNodeProps, TreeNodeState> {
  #initialCollapseState = true;
  constructor(props: TreeNodeProps) {
    super(props);
    this.state = {
      collapsed: this.#initialCollapseState,
    };
    this.toggleCollapsed = this.toggleCollapsed.bind(this);
    this.delete = this.delete.bind(this);
    this.addNodeAfter = this.addNodeAfter.bind(this);
  }

  toggleCollapsed(state: boolean) {
    this.setState({ collapsed: state });
  }

  get collapsed(): boolean {
    return this.state.collapsed;
  }

  set collapsed(value: boolean) {
    this.setState({ collapsed: true });
  }

  delete() {
    this.props.nodeGroup.deleteItem(this.props.id);
  }

  addNodeAfter() {
    this.props.nodeGroup.addItem(this.props.id);
  }

  set selected(value: boolean) {
    if (value) {
      this.props.nodeGroup.selectNode(this.props.id);
    } else {
      this.props.nodeGroup.deselectNode(this.props.id);
    }
  }

  get selected(): boolean {
    return this.props.nodeGroup.isSelected(this.props.id);
  }

  override render(): React.ReactNode {
    const props = this.props;
    const Comp = props.component;
    const data = props.data;

    return (
      <div>
        <div className={styles['nodeFrame']}>
          <TreeNodeHeader
            node={this}
            collapsedProps={
              this.props.children
                ? {
                    collapsed: this.state.collapsed,
                    onToggle: this.toggleCollapsed,
                  }
                : undefined
            }
            treeContext={this.props.treeContext}
          ></TreeNodeHeader>
          <Comp {...data}></Comp>
        </div>
        <div className={styles['childrenFrame']}>
          {props.children && !this.state.collapsed && (
            <NodeGroup
              treeContext={this.props.treeContext}
              items={props.children}
            ></NodeGroup>
          )}
        </div>
      </div>
    );
  }
}

export interface TreeNodeHeaderProps {
  node: TreeNode;
  collapsedProps?: CollapseControlProps;
  treeContext: TreeContext;
  // deleteCallback: () => void;
}

export class TreeNodeHeader extends Component<TreeNodeHeaderProps> {
  get node(): TreeNode {
    return this.props.node;
  }

  override render() {
    return (
      <div className={styles['nodeHeader']}>
        {this.props.collapsedProps ? (
          <CollapseControl {...this.props.collapsedProps}></CollapseControl>
        ) : (
          <CollapseControlDisabled></CollapseControlDisabled>
        )}

        <SelectedControl
          node={this.node}
          selected={this.node.selected}
          selectionMode={this.props.treeContext.selectionMode}
        ></SelectedControl>

        <button
          className={styles['nodeControlButton']}
          onClick={this.node.addNodeAfter}
        >
          +
        </button>
        <button
          className={styles['nodeControlButton']}
          onClick={this.node.delete.bind(this.node)}
        >
          -
        </button>
      </div>
    );
  }
}

interface SelectedControlProps {
  selected: boolean;
  selectionMode: SelectionMode;
  node: TreeNode;
}

function SelectedControl(props: SelectedControlProps) {
  return (
    <>
      {''}
      {props.selectionMode === SelectionMode.Multiple ? (
        <input
          type="checkbox"
          title="selected"
          checked={props.node.selected}
          onChange={(e) => {
            props.node.selected = e.currentTarget.checked;
          }}
        />
      ) : (
        ''
      )}
    </>
  );
}

interface CollapseControlProps {
  collapsed: boolean;
  onToggle: (collapsed: boolean) => void;
}

class CollapseControl extends Component<CollapseControlProps, never> {
  override render(): React.ReactNode {
    const iconClass = this.props.collapsed
      ? 'bi bi-chevron-right'
      : 'bi bi-chevron-down';
    return (
      <i
        className={iconClass}
        onClick={(e) => {
          this.props.onToggle(!this.props.collapsed);
        }}
      ></i>
    );
  }
}

function CollapseControlDisabled() {
  return <i className={'bi bi-dot'}></i>;
}
export interface NodesGroupProps {
  items: TreeNodeData<NodeDataBaseType>[];
  treeContext: TreeContext;
}

interface NodeGroupState {
  selectedNodes: Set<string>;
}

export class NodeGroup extends Component<NodesGroupProps, NodeGroupState> {
  nodeFactory: NodeFactory;
  constructor(props: NodesGroupProps) {
    super(props);
    this.state = { selectedNodes: new Set() };
    this.nodeFactory = props.treeContext.nodeFactory;
  }

  deleteItem(id: TreeNodeId) {
    const ind = this.props.items.findIndex((item) => item.id.equals(id));
    this.props.items.splice(ind, 1);
    this.forceUpdate();
  }

  selectNode(id: TreeNodeId) {
    this.state.selectedNodes.add(id.value());
    this.setState({ selectedNodes: this.state.selectedNodes });
  }

  deselectNode(id: TreeNodeId) {
    this.state.selectedNodes.delete(id.value());
    this.setState({ selectedNodes: this.state.selectedNodes });
  }

  isSelected(id: TreeNodeId): boolean {
    return this.state.selectedNodes.has(id.value());
  }

  addItem(targetNodeId: TreeNodeId) {
    const newId = this.props.treeContext.nodeIdProvider();
    const targetIndex = this.props.items.findIndex(
      (item) => item.id === targetNodeId
    );
    this.props.items.splice(targetIndex + 1, 0, {
      typeName: new NodeTypeName('Group'),
      id: newId,
      data: {},
    });
    this.forceUpdate();
  }

  override render() {
    return this.props.items.map((item) => {
      const { Comp, id, data } =
        this.props.treeContext.nodeFactory.getComponentFromNodeData(item);
      return (
        <div key={item.id.value()}>
          <TreeNode
            id={id}
            component={Comp}
            data={data}
            children={item.children}
            nodeGroup={this}
            treeContext={this.props.treeContext}
            selected={this.state.selectedNodes.has(id.value())}
          />
        </div>
      );
    });
  }
}
