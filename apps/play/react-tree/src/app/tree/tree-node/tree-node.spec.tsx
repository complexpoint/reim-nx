import { render } from '@testing-library/react';
import { nodeDataFromPlainObject } from '../node-types-registry/tree-node-data';
import { defaultNodeFactory } from '../node-types-registry/default-node-factory';
import { TreeNode } from './tree-node';

describe('TreeNode', () => {
  const nodeData = nodeDataFromPlainObject({
    typeName: 'empty',
    id: '1',
    data: {},
  });
  const { Comp, data } = defaultNodeFactory.getComponentFromNodeData(nodeData);
  it('should render successfully', () => {
    const { baseElement } = render(
      <TreeNode component={Comp} id={nodeData.id} data={data} />
    );
    expect(baseElement).toBeTruthy();
  });
});
