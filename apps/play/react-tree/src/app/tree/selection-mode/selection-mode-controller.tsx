import { Component } from 'react';

import { SelectionMode, toggleSelectionMode } from './selection-mode';

interface SelectionModeControllerProps {
  changeCallback: (mode: SelectionMode) => void;
  source: SelectionMode;
  title: string;
}
export class SelectionModeController extends Component<SelectionModeControllerProps> {
  newMode(): SelectionMode {
    return toggleSelectionMode(this.props.source);
  }
  override render() {
    return (
      <>
        <label>{this.props.title}</label>
        <input
          title={this.props.title}
          type="checkbox"
          checked={this.props.source === 'multiple'}
          onChange={(e) => this.props.changeCallback(this.newMode())}
        ></input>
      </>
    );
  }
}
