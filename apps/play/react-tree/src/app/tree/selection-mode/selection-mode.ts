export const SINGLE = 'single'
export enum SelectionMode {
  Single = 'single',
  Multiple = 'multiple'
}
export function toggleSelectionMode(mode: SelectionMode): SelectionMode {
  return mode === SelectionMode.Single ? SelectionMode.Multiple : SelectionMode.Single;
}
