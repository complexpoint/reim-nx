type ValueType = string;

export class TreeNodeId {
  private _value: ValueType;
  constructor(value: ValueType) {
    this._value = value;
  }
  equals(other: TreeNodeId) {
    return this._value === other._value
  }

  value(): ValueType {
    return this._value;
  }
}

export type TreeNodeIdProvider = () => TreeNodeId

