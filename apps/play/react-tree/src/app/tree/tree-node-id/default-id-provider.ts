import { v4 } from 'uuid';
import { TreeNodeIdProvider, TreeNodeId } from "./tree-node-id";


export const defaultTreeNodeIdProvider: TreeNodeIdProvider = function () {
  return new TreeNodeId(v4());
}

