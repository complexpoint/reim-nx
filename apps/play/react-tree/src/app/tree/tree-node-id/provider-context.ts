import { createContext } from 'react';
import { defaultTreeNodeIdProvider } from './default-id-provider';

export const NodeIdProviderContext = createContext(defaultTreeNodeIdProvider);
