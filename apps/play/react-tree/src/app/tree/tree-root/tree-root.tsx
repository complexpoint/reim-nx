import './tree-root.module.css';
import React, { Component } from 'react';
import { NodeGroup } from '../tree-node/tree-node';
import {
  TreeNodeData,
  NodeDataBaseType,
} from '../../node-types-registry/tree-node-data';

import { SelectionModeController } from '../selection-mode/selection-mode-controller';
import { TreeContext } from '../tree-context/tree-context';
export const childrenRootName = 'childrenRoot';
/* eslint-disable-next-line */
export interface TreeRootProps {
  items?: TreeNodeData<NodeDataBaseType>[];
  treeContext: TreeContext;
}

export interface TreeRootState {
  treeContext: TreeContext;
}

export class TreeRoot extends Component<TreeRootProps, TreeRootState> {
  readonly items: Array<TreeNodeData>;

  constructor(props: TreeRootProps) {
    super(props);
    this.items = this.props.items || [];

    this.state = { treeContext: this.props.treeContext };
  }

  get treeContext(): TreeContext {
    return this.state.treeContext;
  }
  setTreeContext(values: Partial<TreeContext>) {
    this.setState({ treeContext: { ...this.treeContext, ...values } });
  }

  override render() {
    return (
      <>
        <TreeRootControlPanel root={this}></TreeRootControlPanel>

        <div className={childrenRootName}>
          <NodeGroup
            items={this.items}
            treeContext={this.treeContext}
          ></NodeGroup>
        </div>
      </>
    );
  }
}

interface TreeRootControlPanelProps {
  root: TreeRoot;
}
class TreeRootControlPanel extends React.Component<TreeRootControlPanelProps> {
  override render(): React.ReactNode {
    return (
      <div>
        <SelectionModeController
          source={this.props.root.treeContext.selectionMode}
          changeCallback={(mode) =>
            this.props.root.setTreeContext({ selectionMode: mode })
          }
          title="Multiple selection"
        ></SelectionModeController>
      </div>
    );
  }
}

export default TreeRoot;
