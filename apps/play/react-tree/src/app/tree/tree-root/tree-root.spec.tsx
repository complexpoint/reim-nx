import { render } from '@testing-library/react';
import { defaultNodeFactory } from '../node-types-registry/default-node-factory';
import TreeRoot, { childrenRootName } from './tree-root';
import { nodeDataFromPlainObject } from '../node-types-registry/tree-node-data';
describe('TreeRoot', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <TreeRoot nodeFactory={defaultNodeFactory} />
    );
    expect(baseElement).toBeTruthy();
  });
  describe('with items', () => {
    // defaultNodeFactory does not recognize 'empty' node type.
    // all these will become error nodes
    const items = [
      { typeName: 'empty', id: '1', data: {} },
      { typeName: 'empty', id: '2', data: {} },
      { typeName: 'empty', id: '3', data: {} },
    ].map(nodeDataFromPlainObject);
    it('should render successfully', () => {
      const { baseElement } = render(
        <TreeRoot items={items} nodeFactory={defaultNodeFactory} />
      );
      expect(baseElement).toBeTruthy();
    });
    it('should have one child for each item', () => {
      const { baseElement } = render(
        <TreeRoot items={items} nodeFactory={defaultNodeFactory} />
      );
      const childrenRoot =
        baseElement.getElementsByClassName(childrenRootName)[0];
      expect(childrenRoot.childNodes).toHaveLength(items.length);
    });
  });
});
