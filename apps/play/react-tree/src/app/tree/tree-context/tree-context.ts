import { NodeFactory } from "../../node-types-registry/node-types-registry";
import { TreeNodeIdProvider } from "./../tree-node-id/tree-node-id";
import { SelectionMode } from "../selection-mode/selection-mode";

export interface TreeContext {
  nodeFactory: NodeFactory;
  nodeIdProvider: TreeNodeIdProvider;
  selectionMode: SelectionMode;
}


