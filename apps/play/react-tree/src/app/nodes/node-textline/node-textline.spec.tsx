import { render } from '@testing-library/react';

import NodeTextline from './node-textline';

describe('NodeTextline', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NodeTextline />);
    expect(baseElement).toBeTruthy();
  });
});
