import { Component } from 'react';

import './node-textline.module.css';

/* eslint-disable-next-line */
export interface NodeTextLineProps {
  text?: string;
}

export class NodeTextLine extends Component<NodeTextLineProps> {
  override render() {
    return (
      <div>
        <div>{this.props.text || 'text line'}</div>
      </div>
    );
  }
}

export default NodeTextLine;
