import { Component } from 'react';

import './node-numeric-counter.module.css';

/* eslint-disable-next-line */
export interface NodeNumericCounterProps {
  title: string;
}

export class NodeNumericCounter extends Component<NodeNumericCounterProps> {
  override render() {
    return (
      <div>
        <label>{this.props.title}: </label>
        <input type="number" title={this.props.title}></input>
      </div>
    );
  }
}

export default NodeNumericCounter;
