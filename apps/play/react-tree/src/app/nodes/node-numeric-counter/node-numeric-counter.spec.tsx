import { render } from '@testing-library/react';

import NodeNumericCounter from './node-numeric-counter';

describe('NodeNumericCounter', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NodeNumericCounter />);
    expect(baseElement).toBeTruthy();
  });
});
