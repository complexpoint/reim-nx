import { render } from '@testing-library/react';

import NodeGroup from './node-group';

describe('NodeGroup', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NodeGroup />);
    expect(baseElement).toBeTruthy();
  });
});
