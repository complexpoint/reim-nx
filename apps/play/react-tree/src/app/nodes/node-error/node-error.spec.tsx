import { render } from '@testing-library/react';

import NodeError from './node-error';

describe('NodeError', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NodeError msg='test' />);
    expect(baseElement).toBeTruthy();
  });
});
