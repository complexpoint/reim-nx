import { JSXElementConstructor } from 'react';

import './node-error.module.css';

/* eslint-disable-next-line */
export interface NodeErrorProps {
  msg: string;
}

export function NodeError(props: NodeErrorProps) {
  return <div>ERROR: {props.msg}</div>;
}

export default NodeError;

export type ErrorNodeType = JSXElementConstructor<NodeErrorProps>;
