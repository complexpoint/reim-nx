import { mathExpressionsSamples } from './math-expressions-samples';

describe('mathExpressionsSamples', () => {
  it('should work', () => {
    expect(mathExpressionsSamples()).toEqual('math-expressions-samples');
  });
});
