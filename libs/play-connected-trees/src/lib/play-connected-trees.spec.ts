import { playConnectedTrees } from './play-connected-trees';

describe('playConnectedTrees', () => {
  it('should work', () => {
    expect(playConnectedTrees()).toEqual('play-connected-trees');
  });
});
