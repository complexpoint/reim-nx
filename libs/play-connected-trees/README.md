# play-connected-trees

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build play-connected-trees` to build the library.

## Running unit tests

Run `nx test play-connected-trees` to execute the unit tests via [Jest](https://jestjs.io).
