import { types } from 'mobx-state-tree'


export const StringData = types
  .model("String", {
    data: types.string
  })

export const NumberData = types
  .model("Number", {
    data: types.number
  })

export const BooleanData = types
  .model("Number", {
    data: types.boolean
  })


export const NodeData = types.union(StringData, NumberData, BooleanData);
