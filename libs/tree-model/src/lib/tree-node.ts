import { types, Instance, getParent, getRoot, getRelativePath, IAnyModelType } from 'mobx-state-tree'

const TreeNodeModelLate = types.late((): IAnyModelType => TreeNodeModel)



// Property Name Editor
class PropNameEditError {
  protected reason = '';
  constructor(private oldName: string, private newName: string) { }
  toString(): string {
    return `error renaming property ${this.oldName} to ${this.newName}.\n${this.reason}`
  }

}

class ValidationError extends PropNameEditError {
  constructor(oldName: string, newName: string, reason: string) {
    super(oldName, newName)
    this.reason = reason;
  }
}



const PropNameEditor = types.model("PropNameEditor", {
  map: types.maybeNull(types.map(TreeNodeModelLate)),
  oldName: types.optional(types.string, ""),
  newName: types.optional(types.string, ""),
})
  .views(self => ({
    isModified(): boolean {
      return self.oldName === self.newName
    },
    validate(): null | ValidationError {
      if (self.map === null) return new ValidationError(self.oldName, self.newName, 'no properties map is provided')
      if (self.map.has(self.newName)) {
        return new ValidationError(self.oldName, self.newName, 'property with new name already exists')
      };
      if (!self.map.has(self.oldName)) return new ValidationError(self.oldName, self.newName, 'no property with the old name');
      return null
    },

  }))
  .actions(self => ({
    clear() {
      self.oldName = ''
      self.newName = ''
    }
  }))
  .actions(self => ({
    setNode(node: TreeNode): void {
      self.newName = node.propertyName();
      self.oldName = self.newName;
      self.map = node.properties.data;
    }
  }))
  .actions(self => ({
    commit(): null | PropNameEditError {

      const validationError = self.validate();
      if (validationError) return validationError;
      if (self.map === null) return validationError; // needed to make ts happy
      const m = self.map;
      const value = m.get(self.oldName);
      m.set(self.newName, value);
      m.delete(self.oldName);
      self.clear();
      return null;
    },
  }))


// Tree Node Map.
// Structure to hold parent's properties as Name-Node pairs.

export type TreeNode = Instance<typeof TreeNodeModelLate>
export type TreeNodesMap = Instance<typeof TreeNodesMapModel>

export const TreeNodesMapModel = types.model('TreeNodesMap', {
  data: types.map(TreeNodeModelLate),
})
  .views(self => ({
    entriesArray(): Array<[string, TreeNode]> {
      return Array.from(self.data.entries());
    }
  }))
  .views(self => ({
    length(): number {
      return self.data.size
    }
  }))
  .views(self => ({
    //TreeNodeModel that owns these properties
    hostNode(): TreeNode {
      return getParent(self) as TreeNode
    }
  }))
  .actions(self => ({
    insert(name: string, node: TreeNode): void {
      self.data.set(name, node)
    },
    remove(name: string): void {
      self.data.delete(name);
    }
  }))
  .actions(self => ({
    new(name: string): void {
      const node = TreeNodeModel.create({
        selected: false,
        data: name,
        collapsed: false,
        properties: TreeNodesMapModel.create(),
      })
      self.insert(name, node)
    },
  }))
  .views(self => {
    let lastIndex = 0;
    function nextFreeName(): string {
      lastIndex++;
      return 'prop' + lastIndex.toString();
    }
    return { nextFreeName }
  })
  .actions(self => {

    function newDefault(): void {
      const name = self.nextFreeName();
      self.new(name);
    }

    return { newDefault }
  })


export const TreeNodeModel = types
  .model("TreeNodeModel", {
    selected: types.boolean,
    data: types.string,
    collapsed: types.boolean,
    properties: TreeNodesMapModel,
  }
  )
  .views(self => ({
    parentNode(): TreeNode {
      return getParent(self, 3) as TreeNode;
    },
    propertyName(): string {
      const propsMap = getParent(self, 1);
      // remove leading '/' from the path
      return getRelativePath(propsMap, self).slice(1);
    },
    hasChildren(): boolean {
      return self.properties.length() > 0;
    }
  }))
  .actions(self => ({ // Event handlers

    // Remove this node from the parent's properties
    delete() {
      self.parentNode().properties.remove(self.propertyName());
    }
  }))
  .actions(self => ({ // selection
    setSelected(value: boolean): void {
      self.selected = value;
    }
  }))
  .actions(self => ({
    setCollapsed(value: boolean): void {
      self.collapsed = value
    },
    toggleCollapsed(): void {
      self.collapsed = !self.collapsed;
    }

  }))
  .views(self => {

    function isInFocus(): boolean {
      const root = getRoot(self) as Tree;
      return getRelativePath(root, self) === root.focusedNodePath;
    }

    return { isInFocus }
  })
  .actions(self => ({
    setFocus(e: Event): void {
      const root = getRoot(self) as Tree;
      root.setFocus(getRelativePath(root, self));
      root.setFocusedNode(self);
      // root.propNameEditor.setNode(self);
      e.stopPropagation();
    },
  }))

export const TreeModel = types.model('Tree', {
  root: TreeNodeModel,
  focusedNode: types.maybeNull(types.reference(TreeNodeModel)),
  focusedNodePath: types.maybeNull(types.string), // path to the focused node
  // propNameEditor: types.optional(PropNameEditor, () => PropNameEditor.create()),
})
  .actions(self => ({
    setFocus(path: string) {
      self.focusedNodePath = path;
    },
    setFocusedNode(node: TreeNode) {
      self.focusedNode = node;
    }
  }))

export type Tree = Instance<typeof TreeModel>

